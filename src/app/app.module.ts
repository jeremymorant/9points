import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';

import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { IonicStorageModule } from '@ionic/storage';
import { Keyboard } from '@ionic-native/keyboard';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { NativeAudio } from '@ionic-native/native-audio';
import { AndroidFullScreen } from '@ionic-native/android-full-screen';

import { MyApp } from './app.component';

import { LoginPage} from '../pages/login/login';
import { LoginTempPage} from '../pages/login-temp/login-temp';
import { SignupPage } from '../pages/signup/signup';
import { ForgotPage } from '../pages/forgot/forgot';

import { TabsPage } from '../pages/tabs/tabs';

import { GameMenuPage} from '../pages/game-menu/game-menu';
import { EditorMenuPage } from '../pages/editor-menu/editor-menu';
import { StatisticsPage } from '../pages/statistics/statistics';
import { GamePlayPage } from '../pages/game-play/game-play';
import { CompteTempPage } from '../pages/compte-temp/compte-temp';
import { ComptePage } from '../pages/compte/compte';
import { ChangeMdpPage } from '../pages/change-mdp/change-mdp';


@NgModule({
  declarations: [
    MyApp,
    TabsPage,
    LoginPage,
    ForgotPage,
    SignupPage,
    GameMenuPage,
    EditorMenuPage,
    GamePlayPage,
    StatisticsPage,
    CompteTempPage,
    LoginTempPage,
    ComptePage,
    ChangeMdpPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp, {
      scrollPadding: false,
      scrollAssist: true,
      autoFocusAssist: false
    }),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    TabsPage,
    LoginPage,
    ForgotPage,
    SignupPage,
    GameMenuPage,
    EditorMenuPage,
    GamePlayPage,
    StatisticsPage,
    CompteTempPage,
    ComptePage,
    LoginTempPage,
    EditorMenuPage,
    ChangeMdpPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Keyboard,
    HttpModule,
    NativeAudio,
    AndroidFullScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
