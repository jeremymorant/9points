import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { Keyboard } from '@ionic-native/keyboard';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AndroidFullScreen } from '@ionic-native/android-full-screen';


import { LoginTempPage } from '../pages/login-temp/login-temp';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = LoginTempPage;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, keyboard: Keyboard, androidFullScreen: AndroidFullScreen) {
    platform.ready().then(() => {
      androidFullScreen.isImmersiveModeSupported()
      .then(() => {
        androidFullScreen.immersiveMode();
        //androidFullScreen.setSystemUiVisibility(AndroidFullScreen.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
      })
      .catch(err => console.log(err));
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      keyboard.disableScroll(true);
      splashScreen.hide();
    });
  }
}
