import { Component } from '@angular/core';
import { Http } from '@angular/http';
import {Md5} from 'ts-md5/dist/md5';
import 'rxjs/add/operator/map';

import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';

import { SignupPage } from '../signup/signup';
import { ForgotPage } from '../forgot/forgot';
import { TabsPage } from '../tabs/tabs';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  forgotPage = ForgotPage;
  signupPage = SignupPage;
  user = {
    email: '',
    password: ''
  };
  userStorage = {
    email: '',
    password: ''
  };

  constructor(public navCtrl: NavController, public navParams: NavParams, private http: Http, private storage : Storage) {
    this.storage.clear();
  }

  ngOnInit() {
    this.init().then((values) =>{
      this.login(this.userStorage.email, this.userStorage.password);
    });
  }

  private init(){

    let promiseList: Promise<any>[] = [];

    promiseList.push(this.storage.get('email').then((val) => { this.userStorage.email = val;}));
    promiseList.push(this.storage.get('password').then((val) => { this.userStorage.password = val;}));

    return Promise.all(promiseList);
  }

  private login(email, password){
    this.http.get('http://dev.juniormiageconcept.com/etu_reg01_am03_2018/api/user/login/?email='+email+'&password='+password, {

    }).map(res => res.json()).subscribe(datas => {
      if(datas.success == "success"){
        this.storage.set('id_user', datas.data.id);
        this.storage.set('pseudo', datas.data.pseudo);
        this.storage.set('nom', datas.data.nom);
        this.storage.set('prenom', datas.data.prenom);
        this.storage.set('email', email);
        this.storage.set('password', password);

        this.navCtrl.setRoot(TabsPage);
      }else{

      }

    });
  }

  public submitLogin(loginForm) {
    let email = loginForm.value.email;
    let password = Md5.hashStr(loginForm.value.password);

    this.login(email, password);
  }

  public overpass(){
    this.navCtrl.setRoot(TabsPage);
  }






}
