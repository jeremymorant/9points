import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';

import { Http } from '@angular/http';
import { Md5 } from 'ts-md5/dist/md5';

/**
 * Generated class for the ComptePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

 @IonicPage()
 @Component({
   selector: 'page-change-mdp',
   templateUrl: 'change-mdp.html',
})
export class ChangeMdpPage {
  user = {
    oldMdp: '',
    newMdp: '',
    newMdpConfirm: ''
  };

  constructor(public navCtrl: NavController, public navParams: NavParams, private storage: Storage, private http: Http) {

  }

  ngOnInit() {

  }

  changeMdp(changeMdpForm){
    let oldMdp = Md5.hashStr(changeMdpForm.value.oldMdp);
    let newMdp = Md5.hashStr(changeMdpForm.value.newMdp);
    let newMdpConfirm = Md5.hashStr(changeMdpForm.value.newMdpConfirm);

    let id_user = '';
    let targetMdp = '';

    let promiseList: Promise<any>[] = [];
    promiseList.push(this.storage.get("id_user").then((value) => {id_user = value;}));
    promiseList.push(this.storage.get("password").then((value) => {targetMdp = value;}));

    Promise.all(promiseList).then((values) => {
      if(oldMdp == targetMdp && newMdp == newMdpConfirm){
        this.http.get('http://dev.juniormiageconcept.com/etu_reg01_am03_2018/api/user/mdp/change/?oldMdp='+oldMdp+'&newMdp='+newMdp+'&newMdpConfirm='+newMdpConfirm+'&id_user='+id_user, {

        }).map(res => res.json()).subscribe(datas => {
          if(datas.success == "success"){
            this.storage.set("password", newMdp);
            this.navCtrl.pop();
          }else{
            // error
          }
        });
      }else{
        // pas bon mdp
      }
    });

  }
}
