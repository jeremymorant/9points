import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ChangeMdpPage } from './change-mdp';

@NgModule({
  declarations: [
  ],
  imports: [
    IonicPageModule.forChild(ChangeMdpPage),
  ],
})
export class ChangeMdpPageModule {}
