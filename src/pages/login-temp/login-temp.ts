import { Component } from '@angular/core';
import { Http } from '@angular/http';
import {Md5} from 'ts-md5/dist/md5';
import 'rxjs/add/operator/map';

import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';

import { SignupPage } from '../signup/signup';
import { ForgotPage } from '../forgot/forgot';
import { TabsPage } from '../tabs/tabs';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login-temp',
  templateUrl: 'login-temp.html',
})
export class LoginTempPage {
  forgotPage = ForgotPage;
  signupPage = SignupPage;
  user = {
    email: '',
    phone: '',
    nom: ''
  };
  userStorage = {
    email: '',
    phone: '',
    nom: ''
  };

  constructor(public navCtrl: NavController, public navParams: NavParams, private http: Http, private storage : Storage) {

  }


  ngOnInit() {
    this.init().then((values) =>{
      if(this.userStorage.email != null && this.userStorage.phone != null && this.userStorage.nom != null){
        this.login(this.userStorage.email, this.userStorage.phone, this.userStorage.nom);
      }else{
        this.storage.clear();
      }
    });
  }

  private init(){

    let promiseList: Promise<any>[] = [];

    promiseList.push(this.storage.get('email').then((val) => { this.userStorage.email = val;}));
    promiseList.push(this.storage.get('phone').then((val) => { this.userStorage.phone = val;}));
    promiseList.push(this.storage.get('nom').then((val) => { this.userStorage.nom = val;}));

    return Promise.all(promiseList);
  }

  private login(email, phone, nom){
    this.http.get('http://dev.juniormiageconcept.com/etu_reg01_am03_2018/api/user/loginTemp/?email='+email+'&phone='+phone+'&nom='+nom, {

    }).map(res => res.json()).subscribe(datas => {
      if(datas.success == "success"){
        this.storage.set('email', email);
        this.storage.set('phone', phone);
        this.storage.set('nom', nom);

        this.navCtrl.setRoot(TabsPage);
      }else{

      }

    });
  }

  public submitLogin(loginForm) {
    let nom = loginForm.value.nom;
    let email = loginForm.value.email;
    let phone = loginForm.value.phone;

    this.login(email, phone, nom);
  }

  public overpass(){
    this.navCtrl.setRoot(TabsPage);
  }






}
