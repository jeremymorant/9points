import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LoginTempPage } from './login-temp';

@NgModule({
  declarations: [
  ],
  imports: [
    IonicPageModule.forChild(LoginTempPage),
  ],
})
export class LoginTempPageModule {}
