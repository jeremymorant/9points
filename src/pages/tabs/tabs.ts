import { Component } from '@angular/core';

import { GameMenuPage } from '../game-menu/game-menu';
import { StatisticsPage } from '../statistics/statistics';
import { CompteTempPage } from '../compte-temp/compte-temp';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = GameMenuPage;
  tab2Root = StatisticsPage;
  tab3Root = CompteTempPage;

  constructor() {

  }
}
