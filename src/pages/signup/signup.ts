import { Component } from '@angular/core';
import { Http } from '@angular/http';
import {Md5} from 'ts-md5/dist/md5';
import 'rxjs/add/operator/map';

import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { TabsPage } from '../tabs/tabs';
/**
 * Generated class for the SignupPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})
export class SignupPage {
  user = {
    email: '',
    password: '',
    passwordConfirm: '',
    nom: '',
    prenom: ''
  };

  constructor(public navCtrl: NavController, public navParams: NavParams, private http: Http) {
  }

  signup(signupForm){
    let email = signupForm.value.email;
    let nom = signupForm.value.nom;
    let prenom = signupForm.value.prenom;
    let password = Md5.hashStr(signupForm.value.password);
    let passwordConfirm = Md5.hashStr(signupForm.value.passwordConfirm);

    this.http.get('http://dev.juniormiageconcept.com/etu_reg01_am03_2018/api/user/create/?email='+email+'&password='+password+'&passwordConfirm='+passwordConfirm+'&nom='+nom+'&prenom='+prenom, {

    }).map(res => res.json()).subscribe(data => {
      if(data.success == "success"){
        this.navCtrl.setRoot(TabsPage);
      }else{
        
      }
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SignupPage');
  }

}
