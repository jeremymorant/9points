import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CompteTempPage } from './compte-temp';

@NgModule({
  declarations: [
  ],
  imports: [
    IonicPageModule.forChild(CompteTempPage),
  ],
})
export class CompteTempPageModule {}
