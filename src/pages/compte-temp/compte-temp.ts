import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, App } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Http } from '@angular/http';

import { LoginTempPage } from '../login-temp/login-temp';
import { ChangeMdpPage } from '../change-mdp/change-mdp';
import { EditorMenuPage } from '../editor-menu/editor-menu';

/**
 * Generated class for the ComptePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-temp-compte',
  templateUrl: 'compte-temp.html',
})
export class CompteTempPage {
  user = {
    email: '',
    password: '',
    nom: '',
    prenom: '',
    pseudo: '',
    phone: ''
  };

  admin = false;


  constructor(public navCtrl: NavController, public navParams: NavParams, private storage: Storage, private app: App, private http: Http) {
  }

  ngOnInit() {
    this.init();
  }

  private init(){

    let promiseList: Promise<any>[] = [];

    promiseList.push(this.storage.get('email').then((val) => { this.user.email = val;}));
    promiseList.push(this.storage.get('nom').then((val) => { this.user.nom = val;}));
    promiseList.push(this.storage.get('phone').then((val) => { this.user.phone = val;}));

    return Promise.all(promiseList);
  }

  goToChangeMdp(){
    this.app.getRootNav().push(ChangeMdpPage);

  }

  goToEditorMenu(){
    this.app.getRootNav().push(EditorMenuPage);

  }

  disconnect(){

      this.storage.remove('id_user');
      this.storage.remove('pseudo');
      this.storage.remove('nom');
      this.storage.remove('prenom');
      this.storage.remove('email');
      this.storage.remove('password');
      this.storage.remove('phone');

      this.app.getRootNav().setRoot(LoginTempPage);
  }
}
