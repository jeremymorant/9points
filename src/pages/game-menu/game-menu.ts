import { Component, ViewChild } from '@angular/core';
import { Slides, IonicPage, NavController, NavParams } from 'ionic-angular';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

import { Storage } from '@ionic/storage';

import { GamePlayPage } from '../game-play/game-play';


/**
 * Generated class for the GameMenuPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-game-menu',
  templateUrl: 'game-menu.html',
})
export class GameMenuPage {
  gamePlayPage: GamePlayPage;

  niveauxDone = {
    niveau1: '',
    niveau2: '',
    niveau3: '',
    niveauBonus: ''
  };

  niveaux: any = [
    {
      'niveau' : '4',
      'visible' : ''
    },
    {
      'niveau' : '3',
      'visible' : ''
    },
    {
      'niveau' : '2',
      'visible' : ''
    },
    {
      'niveau' : '1',
      'visible' : 'visible'
    }
  ];

  constructor(public navCtrl: NavController, public navParams: NavParams, private http: Http, private storage: Storage) {

  }

  ionViewWillEnter(){
    this.init();
  }

  private init(){
    this.storage.get("niveau1").then((value) => {console.log(value)});
    this.storage.get("niveau2").then((value) => {console.log(value)});
    let promiseList: Promise<any>[] = [];

    promiseList.push(this.storage.get('niveau1').then((val) => {
      if(val){
        if(val.done){
          this.niveaux[3].done = val.done;
        }
      }
    }));
    promiseList.push(this.storage.get('niveau2').then((val) => {
      if(val){
        if(val.visible){
          this.niveaux[2].visible = val.visible;
        }
        if(val.done){
          this.niveaux[2].done = val.done;
        }
      }
    }));
    promiseList.push(this.storage.get('niveau3').then((val) => {
      if(val){
        if(val.visible){
          this.niveaux[1].visible = val.visible;
        }
        if(val.done){
          this.niveaux[1].done = val.done;
        }
      }
    }));
    promiseList.push(this.storage.get('niveauBonus').then((val) => {
        if(val){
          if(val.visible){
            this.niveaux[0].visible = val.visible;
          }
          if(val.done){
            this.niveaux[0].done = val.done;
          }
        }
    }));

    return Promise.all(promiseList);
  }

  launchGame(niveau){
    let tempNiveaux = this.niveaux.slice().reverse();
    let val = tempNiveaux[niveau.niveau - 1];
    console.log(val);
      if(val){
        if(!val.done && val.visible){
          this.navCtrl.push(GamePlayPage, {
            niveau: niveau.niveau
          });
        }
      }


  }
}
