import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditorMenuPage } from './editor-menu';

@NgModule({
  declarations: [
  ],
  imports: [
    IonicPageModule.forChild(EditorMenuPage),
  ],
})
export class EditorMenuPageModule {}
