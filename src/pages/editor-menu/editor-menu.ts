import { Component, ViewChild, ElementRef } from '@angular/core';
import { AlertController, IonicPage, NavController, NavParams } from 'ionic-angular';
import { Http } from '@angular/http';

import * as PatternLock from '../../assets/patternLock/patternLock';

import { Storage } from '@ionic/storage';

import * as moment from 'moment';
import jQuery from "jquery";
/**
 * Generated class for the EditorMenuPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-editor-menu',
  templateUrl: 'editor-menu.html',
})
export class EditorMenuPage {
  titre: any;
  points: any;
  pattern: any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {

      let marginLarge = 0;
      if(screen.height < 500){ marginLarge = 9;
      }else if(screen.height < 600){ marginLarge = 13;
      }else if(screen.height < 700){ marginLarge = 16;
      }else if(screen.height < 830){ marginLarge = 20;
      }else if(screen.height < 900){ marginLarge = 22;
      }else if(screen.height < 1200){ marginLarge = 35;
      }else{ marginLarge = 60;}
      this.pattern = null;
      this.pattern = new PatternLock("#patternContainer", {margin:marginLarge});

  }

}
