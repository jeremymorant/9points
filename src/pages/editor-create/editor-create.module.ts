import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditorCreatePage } from './editor-create';

@NgModule({
  declarations: [
    EditorCreatePage,
  ],
  imports: [
    IonicPageModule.forChild(EditorCreatePage),
  ],
})
export class EditorCreatePageModule {}
