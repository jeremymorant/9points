import { Component, ViewChild, NgZone } from '@angular/core';
import { Http } from '@angular/http';
import { Slides, IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the StatisticsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-statistics',
  templateUrl: 'statistics.html',
})
export class StatisticsPage {

  @ViewChild('sliderLabels') sliderLabels: Slides;
  @ViewChild('sliderNiveau') sliderNiveau: Slides;
  @ViewChild('scoreContainer') scoreContainer: any;

  itemsLoaded: boolean = false;
  scores: any[][] = [[]];
  labels: any[][] = [[]];
  zone: NgZone;

  constructor(public navCtrl: NavController, public navParams: NavParams, private http: Http, private storage: Storage) {
    this.zone = new NgZone({ enableLongStackTrace: false });
  }

  ionViewWillEnter(){
    this.itemsLoaded = false;
    this.loadLabels();
  }

  ionViewDidLoad() {
    this.sliderLabels.lockSwipes(true);
    this.sliderNiveau.lockSwipes(true);

  }

  slideNext() {
    this.sliderLabels.lockSwipes(false);
    this.sliderLabels.slideNext(200);
    this.sliderLabels.lockSwipes(true);
    this.sliderNiveau.lockSwipes(false);
    this.sliderNiveau.slideNext(200);
    this.sliderNiveau.lockSwipes(true);
  }
  slidePrev() {
    this.sliderLabels.lockSwipes(false);
    this.sliderLabels.slidePrev(200);
    this.sliderLabels.lockSwipes(true);
    this.sliderNiveau.lockSwipes(false);
    this.sliderNiveau.slidePrev(200);
    this.sliderNiveau.lockSwipes(true);
  }

  loadLabels(){
    this.labels = [[]];
    this.http.get('http://dev.juniormiageconcept.com/etu_reg01_am03_2018/api/game/load/labels/', {

    }).map(res => res.json()).subscribe(datas => {
      if(datas.success == "success"){
        for (let data of datas.data){
          if(!this.labels[data.niveau-1]){
            this.labels[data.niveau-1] = new Array();
          }
          this.labels[data.niveau-1].push(data);
        }
      }else{

      }
      this.sliderLabels.update();
    });
  }

  loadScores(id){
    this.scores = [[]];
    this.http.get('http://dev.juniormiageconcept.com/etu_reg01_am03_2018/api/score/load/all/?id_schema='+id, {

    }).map(res => res.json()).subscribe(datas => {
      this.zone.run(() => {
        if(datas.success == "success"){
            let count = 0;
            for (let data of datas.data){
              count++;
              data.labelBis = data.niveau+"."+count;
              if(!this.scores[count-1]){this.scores[count-1] = new Array();}
              this.scores[count-1] = data;
            }
        }else{

        }
      });
    });
  }

}
