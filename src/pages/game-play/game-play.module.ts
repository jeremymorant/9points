import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GamePlayPage } from './game-play';

@NgModule({
  declarations: [
  ],
  imports: [
    IonicPageModule.forChild(GamePlayPage),
  ],
})
export class GamePlayPageModule {}
