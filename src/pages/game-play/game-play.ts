import { Component, ViewChild, ElementRef } from '@angular/core';
import { AlertController, IonicPage, NavController, NavParams } from 'ionic-angular';
import { Http } from '@angular/http';
import { NativeAudio } from '@ionic-native/native-audio';

import * as PatternLock from '../../assets/patternLock/patternLock';

import { Storage } from '@ionic/storage';

import * as moment from 'moment';
import jQuery from "jquery";


/**
 * Generated class for the GamePlayPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-game-play',
  templateUrl: 'game-play.html',
})
export class GamePlayPage {
  @ViewChild('timer') timerGame: ElementRef;
  @ViewChild('nbTry') nbTry: ElementRef;

  time: any; intervalTimer: any;
  minutes: any; seconds: any; milliseconds: any;
  tabBarElement: any;

  pattern: any;
  patternReference: any;

  id_schemas: any = [];
  schemas: any = [];
  labels: any = [];
  points: any = [];
  rank: any;
  niveau: any;

  schemaLoaded: boolean = false;

  alertGood: any;
  alertReady: any;
  alertBad: any;

  colorLines: string = "white";

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private http: Http,
    private storage : Storage,
    private alertCtrl: AlertController,
    private nativeAudio: NativeAudio
  ) {
    this.tabBarElement = document.querySelector('.tabbar');
    this.niveau = navParams.get("niveau");
    this.rank = 0;

      this.id_schemas = new Array();
      this.points = new Array();
      this.labels = new Array();
      if(this.niveau == 2){
        this.colorLines = "yellow";
      }else if(this.niveau == 2){
        this.colorLines = "blue";
      }else if(this.niveau == 3){
        this.colorLines = "red";
      }
  }

  ngOnInit(){
    this.getSchemas();
  }

  ionViewWillLeave(){
      this.tabBarElement.style.display = 'flex';
      clearInterval(this.intervalTimer);
  }

  ionViewDidLoad() {
    this.tabBarElement.style.display = 'none';

    this.time = moment("03:00:000","mm:ss:SSS");

    this.minutes = this.time.minutes();
    this.seconds = this.time.seconds();
    this.milliseconds = this.time.milliseconds();

    this.alertBad = this.alertCtrl.create({
      title: 'Perdu',
      message: 'Voulez-vous abandonner ou recommencer?',
      enableBackdropDismiss: false,
      buttons: [
        {
          text: 'Abandonner',
          role: 'cancel',
          cssClass: 'alert-failure',
          handler: () => this.goBack()
        },
        {
          text: 'Recommencer',
          handler: () => this.reload()
        }
      ]
    });


      document.querySelectorAll('audio')[0].play();
      this.intervalTimer = this.startTimer();
  }

  private getSchemas(){
    this.http.get('http://dev.juniormiageconcept.com/etu_reg01_am03_2018/api/game/load/niveau/?niveau='+this.niveau, {

    }).map(res => res.json()).subscribe(datas => {
      if(datas.success == "success"){
        let promiseList: Promise<any>[] = [];
        for(let data of datas.data){
          promiseList.push(this.storage.get(data.label).then((value) => {
            if(!value){
              this.id_schemas.push(data.id);
              this.points.push(data.points.replace(/,/g,''));
              this.labels.push(data.label);
            }
          }));
        }

        Promise.all(promiseList).then((values) => {
          this.schemaLoaded = true;
          this.buildPatterns();
        });
      }else{

      }
    });
  }

  private getNextSchema(){
    this.rank++;
    this.pattern.reset();
    this.patternReference.setPattern(this.points[this.rank]);
  }

  private buildPatterns(){
    let radiusSmall = 0;
    if(screen.height < 500){ radiusSmall = 11;
    }else if(screen.height < 600){ radiusSmall = 14;
    }else if(screen.height < 700){ radiusSmall = 17;
    }else if(screen.height < 830){ radiusSmall = 22;
    }else if(screen.height < 900){ radiusSmall = 28;
    }else if(screen.height < 1200){ radiusSmall = 34;
    }else{ radiusSmall = 40;}
    this.patternReference = new PatternLock("#patternReference", {radius:5, margin:radiusSmall, enableSetPattern : true});
    this.patternReference.setPattern(this.points[this.rank]);
    this.patternReference.disable();


    let marginLarge = 0;
    if(screen.height < 500){ marginLarge = 9;
    }else if(screen.height < 600){ marginLarge = 13;
    }else if(screen.height < 700){ marginLarge = 16;
    }else if(screen.height < 830){ marginLarge = 22;
    }else if(screen.height < 900){ marginLarge = 28;
    }else if(screen.height < 1200){ marginLarge = 35;
    }else{ marginLarge = 45;}

    this.pattern = new PatternLock("#patternContainer", {
      margin:marginLarge,
      onDraw: (pattern) => {
        let tempsSucces = this.timerGame.nativeElement.innerText;

        if(tempsSucces.split(":")[0] <= 0 && tempsSucces.split(":")[1] <= 0 && tempsSucces.split(":")[2] <= 0 ){

        }else{
          if(pattern == this.points[this.rank] || pattern.split("").reverse().join("") == this.points[this.rank] ){

            clearInterval(this.intervalTimer);
            document.querySelectorAll('audio')[1].play();

            let id_user;
            let pseudo;
            let essai = this.nbTry.nativeElement.innerText;


            let subtitleAlert = "";
            if(tempsSucces.split(":")[1] >= 24){
              subtitleAlert = "Votre temps est remarquable !";
            }else if(tempsSucces.split(":")[1] >= 15){
              subtitleAlert = "Vous avez été plutôt rapide."
            }else if(tempsSucces.split(":")[1] >= 5){
              subtitleAlert = "Encore un peu et c'était perdu.";
            }else{
              subtitleAlert = "J'ai cru que c'était perdu...";
            }

            this.alertGood = this.alertCtrl.create({
              title: 'Bravo!',
              subTitle: subtitleAlert,
              enableBackdropDismiss: false,
              message: 'Voulez-vous revenir ou continuer?',
              buttons: [
                {
                  text: 'Revenir',
                  role: 'cancel',
                  cssClass: 'alert-success',
                  handler: () => this.goBack()
                },
                {
                  text: 'Suivant',
                  handler: () => this.goNext()
                }
              ]
            });

            this.alertGood.present();
/*            /!\ This.alertGood à enlever si on décommente



            let promiseList: Promise<any>[] = [];

            promiseList.push(this.storage.get("id_user").then((value) => {id_user = value;}));
            promiseList.push(this.storage.get("pseudo").then((value) => {pseudo = value;}));

            Promise.all(promiseList).then((values) => {

              this.storage.set(this.labels[this.rank],{
                  time: tempsSucces,
                  try: essai,
                  done: 'done'
              });

              this.http.get('http://dev.juniormiageconcept.com/etu_reg01_am03_2018/api/score/add/?id_schema='+this.id_schemas[this.rank]+'&id_user='+id_user+'&pseudo='+pseudo+'&temps='+tempsSucces+'&essai='+essai, {

              }).map(res => res.json()).subscribe(datas => {
                  this.alertGood.present();
              });
            });
*/

          }else{
            this.pattern.reset();
            let nb = parseInt(this.nbTry.nativeElement.innerText) + 1;
            if(nb > 9) this.nbTry.nativeElement.innerText = nb;
            else this.nbTry.nativeElement.innerText = "0" + nb;
          }
        }
      }
    });


  }

  private startTimer(){
    this.timerGame.nativeElement.style.color = "black";

      return setInterval(() => {
          this.milliseconds = this.milliseconds-32;
          if(this.milliseconds <= 0){
            this.seconds--;
            this.milliseconds = 999;
          }
          if(this.seconds <= 0){
            this.minutes--;
            this.seconds = 59;
          }
          let milli = "";
          let sec = "";
          if(this.milliseconds < 10) milli = "00"+this.milliseconds;
          else if(this.milliseconds < 100) milli = "0"+this.milliseconds;
          else milli = this.milliseconds;
          if(this.seconds < 10) sec = "0"+this.seconds;
          else sec = this.seconds;

          this.timerGame.nativeElement.innerText = "0"+this.minutes+":"+sec+":"+milli;

          if(this.minutes < 2 && this.seconds == 59 && this.milliseconds > 700){
            let dede = document.getElementsByClassName('game-play')[0];
            console.log(dede);
             dede.id = "danger-1";
          }else if(this.minutes < 1 && this.seconds == 59 && this.milliseconds > 700){
            let dede = document.getElementsByClassName('game-play')[0];
            console.log(dede);
             dede.id = "danger-2";
          }

          if(this.intervalTimer != null){
            if(this.milliseconds == 999 && this.seconds == 59 && this.minutes < 0){
              this.timerGame.nativeElement.innerText = "00:00:000";
              this.stopTimer();
            }

          }
      }, 32);
  }

  stopTimer(){

    clearInterval(this.intervalTimer);
    document.querySelectorAll('audio')[1].play();
    this.alertBad.present();

  }

  goBack(){
    this.navCtrl.pop();
  }

  goNext(){
    if(this.rank >= this.labels.length - 1)
    {
      this.alertReady = null;
      this.storage.set("niveau"+this.niveau,{
          visible: "visible",
          done: "done"
      });
      let newLvl = parseInt(this.niveau) + 1;

      this.storage.set("niveau"+newLvl,{
          visible: "visible"
      });
      this.goBack();
    }
    else
    {
      this.getNextSchema();
      this.reload();
    }
  }

  reload(){
    this.nbTry.nativeElement.innerText = "00";
    this.timerGame.nativeElement.style.color = "black";
    this.ionViewDidLoad();
  }
}
